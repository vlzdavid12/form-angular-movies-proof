import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {LayoutModule} from '@angular/cdk/layout';
import {MaterialModule} from "./material/material.module";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavegationComponent} from './navegation/navegation.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TableMoviesComponent} from './table-movies/table-movies.component';
import {PageNofoundComponent} from './page-nofound/page-nofound.component';
import {ContactFormModuleModule} from "./contact-form-module/contact-form-module.module";
import {WellcomeComponent} from './wellcome/wellcome.component';

@NgModule({
  declarations: [
    AppComponent,
    NavegationComponent,
    DashboardComponent,
    TableMoviesComponent,
    PageNofoundComponent,
    WellcomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ContactFormModuleModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
