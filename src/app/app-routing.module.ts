import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TableMoviesComponent} from "./table-movies/table-movies.component";
import {PageNofoundComponent} from "./page-nofound/page-nofound.component";
import {WellcomeComponent} from "./wellcome/wellcome.component";

const routes: Routes = [
  {path: '', component: WellcomeComponent},
  {path: 'home', component: TableMoviesComponent},
  {path: 'form',  loadChildren: () => import('./contact-form-module/contact-form-module.module').then(m => m.ContactFormModuleModule)  },
  { path: '',   redirectTo: '', pathMatch: 'full' },
  { path: '**', component: PageNofoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
