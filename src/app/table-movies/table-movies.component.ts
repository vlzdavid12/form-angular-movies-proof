import {LiveAnnouncer} from '@angular/cdk/a11y';
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {delay, map, tap} from "rxjs/operators";
import {MoviesService} from "../services/movies.service";
import {MovieInterface} from "../interface/movie.interface";

@Component({
  selector: 'app-table-movies',
  templateUrl: './table-movies.component.html',
  styleUrls: ['./table-movies.component.sass']
})
export class TableMoviesComponent implements  OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'title', 'year', 'cast', 'genres', 'options'];
  dataSource!: MatTableDataSource<MovieInterface>;
  time: any;
  loading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  constructor(private moviesService: MoviesService,
              private _liveAnnouncer: LiveAnnouncer) {}

  ngOnInit(): void {
    this.moviesService.getMovies()
      .pipe(
        map(movie => movie.map((item, index) => ({...item, id: index + 1 }))),
        delay(1000)
      )
      .subscribe({
      next: (resp) =>  {
        this.dataSource = new MatTableDataSource<MovieInterface>(resp);
      },complete:()=>{
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.loading = false;
        }})
  }

  /** Announce the change in sort state for assistive technology. */
  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
  ngAfterViewInit() {}

}
