import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {MovieInterface} from "../interface/movie.interface";

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  public url: string = '/prust/wikipedia-movie-data/master/movies.json';

  constructor(private http: HttpClient) { }

  getMovies(): Observable<MovieInterface[]>{
    return this.http.get<MovieInterface[]>(`${environment.apiURL}${this.url}`);
  }
}
