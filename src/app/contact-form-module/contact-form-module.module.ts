import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../material/material.module";
import {ContactFormModuleRoutingModule} from './contact-form-module-routing.module';
import {ContactFormComponentComponent} from "./contact-form-component/contact-form-component.component";
import {PhoneMaskDirective} from "../directive/phone-mask.directive";


@NgModule({
  declarations: [
    ContactFormComponentComponent,
    PhoneMaskDirective
  ],
  exports: [
    PhoneMaskDirective
  ],
  imports: [
    CommonModule,
    ContactFormModuleRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ]
})
export class ContactFormModuleModule {
}
