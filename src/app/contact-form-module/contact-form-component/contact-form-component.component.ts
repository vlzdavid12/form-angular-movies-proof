import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-contact-form-component',
  templateUrl: './contact-form-component.component.html',
  styleUrls: ['./contact-form-component.component.sass']
})
export class ContactFormComponentComponent implements OnInit {
  contactForm!: FormGroup;

  account_validation_messages = {
    'name': [
      { type: 'required', message: 'Username is required' },
      { type: 'minlength', message: 'Username must be at least 5 characters long' },
      { type: 'maxlength', message: 'Username cannot be more than 25 characters long' },
      { type: 'pattern', message: 'Your username must contain only numbers and letters' },
      { type: 'validUsername', message: 'Your username has already been taken' }
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required' }
    ],
    'terms': [
      { type: 'required', message: 'You must accept terms and conditions' }
    ]
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.contactForm = this.fb.group({
      name: ['', Validators.compose([
        Validators.maxLength(25),
        Validators.minLength(5),
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        Validators.required
      ])],
      email:['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])],
      phone:['', Validators.compose([
        Validators.required
      ])],
      comment:[''],
      terms:[true, Validators.compose([
        Validators.required
      ])]
    });
  }


  validateInput(inputName: string, validation: any){
    return this.contactForm.controls[inputName].hasError(validation.type)  && (this.contactForm.controls[inputName].dirty || this.contactForm.controls[inputName].touched);
  }

  submitForm(){
    if(this.contactForm.valid){
      let value = this.contactForm.value;
      console.log(value);
    }
  }

}
